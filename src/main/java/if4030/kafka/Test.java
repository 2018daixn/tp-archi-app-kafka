package if4030.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Produced;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class Test {

    public static final String LINES_STREAM = "lines-stream";
    public static final String WORDS_STREAM = "words-stream";
	public static final String TAGGED_WORDS_STREAM = "tagged-words-stream";
    public static final String COMMAND_TOPIC = "command-topic";
	public static final Pattern pattern = Pattern.compile("^([a-z]|-)*$");
    public static HashMap<String, String> lexiconDict = new HashMap<String, String>();
    public static HashMap<String, Integer> occurenceDict = new HashMap<String, Integer>();

    static Properties getStreamsConfig(final String[] args) throws IOException {
        final Properties props = new Properties();
        if (args != null && args.length > 0) {
            try (final FileInputStream fis = new FileInputStream(args[0])) {
                props.load(fis);
            }
            if (args.length > 1) {
                System.out.println("Warning: Some command line arguments were ignored. This demo only accepts an optional configuration file.");
            }
        }
        props.putIfAbsent(StreamsConfig.APPLICATION_ID_CONFIG, "streams-test");
        props.putIfAbsent(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.putIfAbsent(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        props.putIfAbsent(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.putIfAbsent(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

        // setting offset reset to earliest so that we can re-run the demo code with the same pre-loaded data
        // Note: To re-run the demo, you need to use the offset reset tool:
        // https://cwiki.apache.org/confluence/display/KAFKA/Kafka+Streams+Application+Reset+Tool
        props.putIfAbsent(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return props;
    }

    static void createDictionnary () throws IOException {
        File file = new File("/config/workspace/ressources/Lexique383.tsv");
        FileReader fileReader;
        BufferedReader reader;
        try {
            fileReader = new FileReader(file);
            reader = new BufferedReader(fileReader);
            
            String line = null;
            List<String> types = Arrays.asList("NOM", "VER", "ADV");
            
            while((line = reader.readLine())!= null) {
                List<String> splitLine = Arrays.asList(line.split("\t"));
                if (types.contains(splitLine.get(3))) {
                    lexiconDict.put(splitLine.get(0), splitLine.get(2));
                }
            }

            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Number of lines " + String.valueOf(lexiconDict.size()));
    }

	static List<String> splitLinesAndFilter(String line) {
		List<String> lowerCase = Arrays.asList(line.toLowerCase(Locale.getDefault()).split(" "));

		List<String> filtered = new ArrayList<>();
        for (String word : lowerCase) {
			Matcher matcher = pattern.matcher(word);
			if (matcher.matches()) {
				filtered.add(word);
			}
        }
		return filtered;
	}


    static void createWordStream(final StreamsBuilder builder) {
        final KStream<String, String> source = builder.stream(LINES_STREAM);
        final KStream<String, String> words = source.flatMapValues(value -> splitLinesAndFilter(value));
        words.to(WORDS_STREAM);
    }

    static void createTaggedStream(final StreamsBuilder builder) {
        final KStream<String, String> source = builder.stream(WORDS_STREAM);
        // source.peek((key, value) ->  System.out.println("Received word: " + value));
        final KStream<String, String> taggedWords = source.mapValues(value -> {
            String taggedValue = lexiconDict.get(value);
            return taggedValue;
        });

        taggedWords.to(TAGGED_WORDS_STREAM);
    }

    private static Map<String, Integer> sortByValue(Map<String, Integer> unsortMap) {

        // 1. Convert Map to List of Map
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        /*
        //classic iterator example
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext(); ) {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }*/


        return sortedMap;
    }

    static void getHighestOccurences(final StreamsBuilder builder, Integer amount) {
        final KStream<String, String> source = builder.stream(TAGGED_WORDS_STREAM);
        source.foreach((key, value) -> {
            int occurences = 0;
            Integer readValue = occurenceDict.get(value);
            if (readValue != null) {
                occurences += readValue.intValue();
            }
            occurenceDict.put(value, occurences + 1);
            // System.out.println(value);
          });
        final KStream<String, String> commandSource = builder.stream(COMMAND_TOPIC);
        commandSource.foreach((key, value) -> {
            if (value.compareTo("END") == 0) {
                Map<String, Integer> sortedMap = sortByValue(occurenceDict);
                int i = 0;
                for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
                    if (i > 20 ) {
                        break;
                    }
                    System.out.println("Mot : " + entry.getKey()
                            + "\n Occurrences : " + entry.getValue());
                }
            }
        });
    }

    public static void main(final String[] args) throws IOException {
        final Properties props = getStreamsConfig(args);

        // Creating Lexicon dictionnary
        createDictionnary();

        final StreamsBuilder streamBuilder = new StreamsBuilder();

		// Creating words-stream
        createWordStream(streamBuilder);
        
        //Creating tagged-words-stream
        createTaggedStream(streamBuilder);

        //Creating occurence-stream 
        getHighestOccurences(streamBuilder, 20);

        
        final KafkaStreams streams = new KafkaStreams(streamBuilder.build(), props);
        final CountDownLatch latch = new CountDownLatch(1);


        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-wordcount-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (final Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}
